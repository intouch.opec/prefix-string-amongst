function solution(array) {
  if (array.length === 0) return "";

  const itemSmallestChar = array.reduce((a, b) =>
    a.length < b.length ? a : b
  );

  for (let i = 0; i < itemSmallestChar.length; i++) {
    let char = itemSmallestChar[i];
    for (const item of array) {
      if (item[i] !== char) {
        return itemSmallestChar.slice(0, i);
      }
    }
  }

  return itemSmallestChar;
}

console.log(solution(["flower", "flow", "flight"]));
console.log(solution(["dog", "racecar", "car"])); 